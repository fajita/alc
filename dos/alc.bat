@echo off

set IE=iexplore
set FF="C:\Program Files\Mozilla Firefox\firefox.exe"
set CH="C:\Program Files\Google\Chrome\Application\chrome.exe"

set URL=http://eow.alc.co.jp


set BROWSER=%CH%


if not "%1" == "" (
  set URL="%URL%/search?q=%1%&ref=sa"
) else (
  set URL=http://eow.alc.co.jp  
)

%BROWSER% %URL%

@echo on

